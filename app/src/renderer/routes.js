export default [
    {
    path: '/',
    name: 'landing-page',
    component: require('components/TodoListPageView')
    },
    {
      path: '/todolist/:id',
      name: 'todolist',
      component: require('components/TodoListDetialPageView')
    },
    {
        path: '*',
        redirect: '/'
    }
]
