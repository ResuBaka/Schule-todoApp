const state = {
    todolist : [],
    count:  0
}

const mutations = {
    addTodolist(state, todolist ) {
        // mutate state
        state.todolist.push( {name: todolist.name ,beschreibung: todolist.beschreibung , status: todolist.status} )

        state.count = state.todolist.length
    },
}

export default {
    state,
    mutations
}
